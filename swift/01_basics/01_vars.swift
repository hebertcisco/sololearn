/*
The keyword var is used to declare
a variable.
*/
var num = 42
var bool = true
var string = "My string"

/*
The variable value is possible to change:
*/

num = 1
bool= false
string = "Changed"