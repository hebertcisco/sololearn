let myText = "Hello World"

//Print text is easy
print("Hello World")

/*
To print the value of a variable 
inside a text place it in parentheses,
and  insert a backslash just prior
to the opening parenthesis.
*/
print("The text is: \(myText)")

/*
You do not need the blackslash 
for printing only the variable value:
*/
print(myText)