/*
The keyword var is used to declare
a variable.
*/
let num = 42
let bool = true
let string = "My string"

/*
# The variable value do not is possible to change
*/