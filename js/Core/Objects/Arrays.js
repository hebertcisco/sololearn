//1ª way to create an Array
let courses = new Array("html", "css", "js");
//console.table(courses);

//2ª way to create an Array
let courses2 = new Array();
courses2[0] = "c";
courses2[1] = "swift";
courses2[2] = "c++";
//console.table(courses2);

//3ª way to create an Array: Array Literals
let courses3 = ["kotlin", "java", "xml"];

//The length Property
/*
JavaScript arrays have useful built-in properties and methods.
An array's length property returns the number of it's elements. 
*/
//Example:
console.log(courses3.length);
